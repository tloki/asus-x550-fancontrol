# asus-x550-fancontrol

Fan control curve configuration for Asus notebooks

1) put fancontrol in /etc/fancontrol


2) try:

sudo /usr/sbin/fancontrol

stress a notebook somewhat to ensure fans do spin

3) enable fancontrol service on startup:

sudo service fancontrol enable

4) start it in current session by:

sudo service fancontrol start

5) enjoy

## use at your own risk! 
